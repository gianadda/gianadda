package org.tastefuljava.gianadda.catalog;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.Properties;
import org.tastefuljava.gianadda.util.Configuration;
import org.tastefuljava.jedo.Session;
import org.tastefuljava.jedo.SessionFactory;
import org.tastefuljava.jedo.conf.SessionFactoryBuilder;

public class Catalog implements Closeable {
    private final File dir;
    private final Configuration conf;
    private final SessionFactory factory;
    private final int originalVersion;

    public static Catalog open(File dir, Configuration link) throws IOException {
        return new CatalogBuilder(dir, link).open();
    }

    Catalog(File dir, Configuration conf, int originalVersion) {
        this.dir = dir;
        this.conf = conf;
        this.factory = createSessionFactory(conf);
        this.originalVersion = originalVersion;
    }

    @Override
    public void close() {
        // nothing
    }

    public File getDirectory() {
        return dir;
    }

    public Configuration getConf() {
        return conf;
    }

    public int getOriginalVersion() {
        return originalVersion;
    }

    public Session openSession() {
        return factory.openSession();
    }

    private static SessionFactory createSessionFactory(Configuration conf) {
        Properties props = new Properties();
        props.put("driver", conf.getString("jdbc.driver", null));
        props.put("url", conf.getString("jdbc.url", null));
        props.put("user", conf.getString("jdbc.username", null));
        props.put("password", conf.getString("jdbc.password", null));
        SessionFactoryBuilder builder = new SessionFactoryBuilder(props);
        builder.loadMapping(Catalog.class.getResource("mapping.xml"));
        return builder.build();
    }
}
