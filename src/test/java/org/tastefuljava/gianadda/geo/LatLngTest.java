package org.tastefuljava.gianadda.geo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class LatLngTest {
    
    public LatLngTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testToString() {
        LatLng latLng = new LatLng(22.29924,73.16584);
        assertEquals("22°17'57.264\"N 73°09'57.024\"E", latLng.toString());
    }
}
